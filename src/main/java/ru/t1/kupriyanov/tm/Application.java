package ru.t1.kupriyanov.tm;

import ru.t1.kupriyanov.tm.constant.ArgumentConst;
import ru.t1.kupriyanov.tm.constant.CommandConst;
import ru.t1.kupriyanov.tm.model.Command;
import ru.t1.kupriyanov.tm.repository.CommandRepository;
import ru.t1.kupriyanov.tm.util.FormatUtil;

import java.util.Scanner;

import static ru.t1.kupriyanov.tm.constant.CommandConst.*;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.INFO:
                showInfo();
                break;
            case CommandConst.COMMANDS:
                showCommands();
                break;
            case CommandConst.ARGUMENTS:
                showArguments();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " +  maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " +  FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " +  FormatUtil.formatBytes(usageMemory));
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
        System.exit(1);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    private static void showCommands() {
        System.out.println("[COMMANDS]");
        Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showArguments() {
        System.out.println("[ARGUMENTS]");
        Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anton Kupriyanov");
        System.out.println("E-mail: ankupr29@gmail.com");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

}
